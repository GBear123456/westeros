/*eslint-disable*/
import React from "react";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";

import Search from "@material-ui/icons/Search";
// core components
import CustomInput from "components/CustomInput/CustomInput.js";
import Button from "components/CustomButtons/Button.js";

import styles from "assets/jss/material-dashboard-react/components/headerLinksStyle.js";

const useStyles = makeStyles(styles);

export default function AdminNavbarLinks() {
  const classes = useStyles();

  return (
    <div>
      <div className={classes.searchWrapper}>
        <div className="search-div" style={{width: "300px", background: "#FFF", borderRadius: "30px", position: "relative"}}>
          <input className="search-input-div" style={{background: "transparent", border: "0px", padding: "12px 25px", fontSize: "18px"}} placeholder="Type to Search...">
          </input>
          <ion-icon name="search-outline" style={{position: "absolute", right: "20px", top: "calc(50% - 10px)", width: "20px", height: "20px"}}></ion-icon>
        </div>
      </div>
    </div>
  );
}
