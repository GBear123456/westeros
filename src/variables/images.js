/*eslint-disable*/
import item1 from "../assets/img/items/item1.png";
import item2 from "../assets/img/items/item2.png";
import item3 from "../assets/img/items/item3.png";
import item4 from "../assets/img/items/item4.png";
import item5 from "../assets/img/items/item5.png";
import item6 from "../assets/img/items/item6.png";
import item7 from "../assets/img/items/item7.png";
import item8 from "../assets/img/items/item8.png";
import item9 from "../assets/img/items/item9.png";
import item10 from "../assets/img/items/item10.png";
import item11 from "../assets/img/items/item11.png";
import item12 from "../assets/img/items/item12.png";
import item13 from "../assets/img/items/item13.png";
import item14 from "../assets/img/items/item14.png";
import item15 from "../assets/img/items/item15.png";

export const images = [
  {
    src: item1,
    title: "lorem ipsum",
    author: "by random author",
  },
  {
    src: item2,
    title: "lorem ipsum",
    author: "by random author",
  },
  {
    src: item3,
    title: "lorem ipsum",
    author: "by random author",
  },
  {
    src: item4,
    title: "lorem ipsum",
    author: "by random author",
  },
  {
    src: item5,
    title: "lorem ipsum",
    author: "by random author",
  },
  {
    src: item6,
    title: "lorem ipsum",
    author: "by random author",
  },
  {
    src: item7,
    title: "lorem ipsum",
    author: "by random author",
  },
  {
    src: item8,
    title: "lorem ipsum",
    author: "by random author",
  },
  {
    src: item9,
    title: "lorem ipsum",
    author: "by random author",
  },
  {
    src: item10,
    title: "lorem ipsum",
    author: "by random author",
  },
  {
    src: item11,
    title: "lorem ipsum",
    author: "by random author",
  },
  {
    src: item12,
    title: "lorem ipsum",
    author: "by random author",
  },
  {
    src: item13,
    title: "lorem ipsum",
    author: "by random author",
  },
  {
    src: item14,
    title: "lorem ipsum",
    author: "by random author",
  },
  {
    src: item15,
    title: "lorem ipsum",
    author: "by random author",
  },
];
