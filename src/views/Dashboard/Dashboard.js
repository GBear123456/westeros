/*eslint-disable*/
import React from "react";
// react plugin for creating charts
// @material-ui/core
import { makeStyles } from "@material-ui/core/styles";
import Icon from "@material-ui/core/Icon";
// @material-ui/icons
import Warning from "@material-ui/icons/Warning";
// core components
import GridItem from "components/Grid/GridItem.js";
import GridContainer from "components/Grid/GridContainer.js";
import Danger from "components/Typography/Danger.js";
import Card from "components/Card/Card.js";
import CardBody from "components/Card/CardBody.js";
import CardIcon from "components/Card/CardIcon.js";
import CardFooter from "components/Card/CardFooter.js";
import { images } from "../../variables/images.js";

import styles from "assets/jss/material-dashboard-react/views/dashboardStyle.js";
//import item1 from "../../assets/img/items/item1.png";

const useStyles = makeStyles(styles);

export default function Dashboard() {
  const classes = useStyles();
  return (
    <div>
      <div class="grid-title-self" 
        style={{
          padding: "22px 27px", 
          flex: "1", 
          marginBottom: "32px", 
          borderBottom: "1px solid #59595940",
          fontSize: "23px",
          fontWeight: "600",
          color: "#777777"
          }}>
        Products
      </div>
      <GridContainer>
        {
          images.map((img) => {
            return (
              <GridItem>
                <Card>
                  <CardBody color="warning" stats icon>
                    <div className="card-body-outlet" style={{padding: '15px'}}>
                      <div className="card-body-image" style={{width: "270px", height: "225px"}}>
                        <img src={img.src} alt="card-image" style={{objectFit:"cover", width: "100%", height: "100%"}}></img>
                      </div>
                      <p className={classes.cardCategory} style={{alignSelf: "flex-start"}}>{img.title}</p>
                      <h3 className={classes.cardTitle} style={{alignSelf: "flex-start"}}>
                        {img.author}
                      </h3>
                    </div>
                  </CardBody>
                </Card>
              </GridItem>
            )
          })
        }
        
      </GridContainer>
    </div>
  );
}
